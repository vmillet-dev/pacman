/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package launcher;

import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import outils.ProgressBarBox;
import pacman.Pacman;

/**
 *
 * @author floen
 */
public class LauncherScene extends Scene {
    
    private Image imageLauncher;
    private Button buttonPlayClassic, buttonPlayCustom, buttonQuitter;
    private Pacman pacman;
    private GridPane gPane;
    
    public LauncherScene(GridPane gPane, Pacman pacman) {
        super(gPane);
        this.gPane = gPane;
        this.imageLauncher = new Image(getClass().getResource("/assets/pacman_launcher.png").toString());
        this.buttonPlayClassic = new Button();
        this.buttonPlayCustom = new Button();
        this.buttonQuitter = new Button();
        this.pacman = pacman;
        this.initLauncher();
    }
    
    /**
     * plactement des différents éléments du launcher
     */
    private void initLauncher() {
        this.gPane.add(new ImageView(this.imageLauncher), 0, 0, 3, 1);
        
        ProgressBarBox progressBar = new ProgressBarBox();
        this.gPane.add(progressBar, 0, 1, 3, 1);
        gPane.setHalignment(progressBar, HPos.CENTER);
        progressBar.getThreadProgressBar().start();
        
        progressBar.getThreadProgressBar().setOnSucceeded(event -> {
            this.buttonPlayClassic.disableProperty().setValue(Boolean.FALSE);
            this.buttonPlayCustom.disableProperty().setValue(Boolean.FALSE);
            this.buttonQuitter.disableProperty().setValue(Boolean.FALSE);
            
        });
        
        this.buttonPlayClassic.setText("Labyrinthe Classique");
        this.buttonPlayClassic.setMinWidth(60);
        this.buttonPlayClassic.disableProperty().setValue(Boolean.TRUE);
        this.buttonPlayCustom.setText("Labyrinthe Fait-maison");
        this.buttonPlayCustom.setMinWidth(60);
        this.buttonPlayCustom.disableProperty().setValue(Boolean.TRUE);
        this.buttonQuitter.setText("Quitter");
        this.buttonQuitter.setMinWidth(60);
        this.buttonQuitter.disableProperty().setValue(Boolean.TRUE);
        
        this.buttonPlayClassic.addEventHandler(MouseEvent.MOUSE_CLICKED,
                new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                pacman.getJeu().setIsClassicMaze(true);
                pacman.startGame(pacman.getPrimaryStage());
            }
        });
        
        this.buttonPlayCustom.addEventHandler(MouseEvent.MOUSE_CLICKED,
                new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                pacman.getJeu().setIsClassicMaze(false);
                pacman.startGame(pacman.getPrimaryStage());
            }
        });
        
        this.buttonQuitter.addEventHandler(MouseEvent.MOUSE_CLICKED,
                new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                System.exit(0);
            }
        });
        
        this.gPane.add(this.buttonPlayClassic, 0, 2);
        this.gPane.add(this.buttonPlayCustom, 1, 2);
        this.gPane.add(this.buttonQuitter, 2, 2);
        
    }
}
