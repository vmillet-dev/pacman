/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package outils;

import java.util.ArrayList;
import modele.Position;

/**
 *
 * @author Florent
 */
public class FormTetris {
    
    private ArrayList<Position> listElement;

    public FormTetris(ArrayList<Position> listElement){
        this.listElement = listElement;
    }
    
    /**
     * renvoie lun élément symétrique par le côté droit
     * @return 
     */
    public FormTetris getSysmetriqueDroite(){
        ArrayList<Position> newListElement = new ArrayList();
        this.listElement.forEach((position) -> {
            newListElement.add(new Position(this.getLastLine() - position.getX() + this.getFirstLine(),
                    position.getY()));
        });
        return new FormTetris(newListElement);
    }
    
    /**
     * renvoie lun élément symétrique par le bas
     * @return 
     */
    public FormTetris getSysmetriqueBas(){
        ArrayList<Position> newListElement = new ArrayList();
        this.listElement.forEach((position) -> {
            newListElement.add(new Position(position.getX(),
                    (this.getLastColumn() - position.getY() + this.getFirstColumn())));
        });
        return new FormTetris(newListElement);
    }
    
    /**
     * Distance en x de point le plus haut et de celui le plus bas
     * @return 
     */
    public int getLength(){
        int minX = this.listElement.get(0).getX();
        int maxX = this.listElement.get(0).getX();
        for(Position position : this.listElement){
            if(minX > position.getX())
                minX = position.getX();
            if(maxX < position.getX())
                maxX = position.getX();
        }
        return maxX - minX;
    }
    
    /**
     * Distance en y de point le plus à gauche et de celui le plus à droite
     * @return 
     */
    public int getWidth(){
        int minY = this.listElement.get(0).getY();
        int maxY = this.listElement.get(0).getY();
        for(Position position : this.listElement){
            if(minY > position.getX())
                minY = position.getX();
            if(maxY < position.getX())
                maxY = position.getX();
        }
        return maxY - minY;
    }
    
    /**
     * Obtention du y du point le plus bas en y
     * @return 
     */
    private int getLastColumn(){
        int y = this.listElement.get(0).getY();
        for(Position position : this.listElement){
            y = (y < position.getY() ? position.getY() : y );
        }
        return y;
    }
    
    /**
     * Obtention du y du point le plus haut en y
     * @return 
     */
    private int getFirstColumn(){
        int y = this.listElement.get(0).getY();
        for(Position position : this.listElement){
            y = (y > position.getY() ? position.getY() : y );
        }
        return y;
    }
    
    /**
     * Obtention du x du point le plus à droite en x
     * @return 
     */
    private int getLastLine(){
        int x = this.listElement.get(0).getX();
        for(Position position : this.listElement){
            x = (x < position.getX() ? position.getY() : x );
        }
        return x;
    }
    
    /**
     * Obtention du x du point le plus à gauche en x
     * @return 
     */
    private int getFirstLine(){
        int x = this.listElement.get(0).getX();
        for(Position position : this.listElement){
            x = (x > position.getX() ? position.getY() : x );
        }
        return x;
    }
    
    /**
     * Génération d'une forme en l
     * @param x
     * @param y
     * @param size
     * @return 
     */
    public static FormTetris getFormL(int x, int y, int size){
       ArrayList<Position> listPosition = new ArrayList<>();
       int i;
       for(i = 0; i <= (2*size + 1); i++){
            listPosition.add(new Position(x + i,y));
            listPosition.add(new Position(x + i,y + 1));
       }
       listPosition.add(new Position(x + i - 2,y + 2));
       listPosition.add(new Position(x + i - 2,y + 3));
       listPosition.add(new Position(x + i - 1,y + 2));
       listPosition.add(new Position(x + i - 1,y + 3));
       return new FormTetris(listPosition); 
    }
    
    /**
     * Génération d'une forme basique
     * @param x
     * @param y
     * @return 
     */
    public static FormTetris getSimpleFormPoint(int x,int y){
        ArrayList<Position> listPosition = new ArrayList<>();
       listPosition.add(new Position(x,y));
       listPosition.add(new Position(x+1,y));
       listPosition.add(new Position(x,y+1));
       listPosition.add(new Position(x+1,y+1));
       return new FormTetris(listPosition); 
    }
    
    /**
     * Retourne vrai si la position src existe dans positonDispo
     * @param src
     * @param positionDispo
     * @return 
     */
    private boolean containsPosition(Position src, ArrayList<Position> positionDispo ){
        return (positionDispo.stream().anyMatch((position) -> (src.equals(position))));
    }
    
    /**
     * Retourne vrai si la position est contenu dans la liste de position de la form tetris
     * @param src
     * @return 
     */
    public boolean containsPosition(Position src){
        return !this.listElement.stream().noneMatch((position) -> (src.equals(position)));
    }
    
    /**
     * Retourne vrai si toutes les positions de l'élément existe dans la liste de position dispo
     * @param positionDispo
     * @return 
     */
    public boolean isInsertable(ArrayList<Position> positionDispo){
        return (this.listElement.stream().allMatch((position) -> ( this.containsPosition(position,positionDispo))));
    }
    
    /**
     * Decal tout les carrés d'un en x
     * @param decalValue 
     */
    public void decalX(int decalValue){
        this.listElement.forEach((position) -> {
            position.setX(position.getX() + decalValue);
        });
    }
    
    /**
     * Decal tout les carrés d'un en y
     * @param decalValue 
     */
    public void decalY(int decalValue){
        this.listElement.forEach((position) -> {
            position.setY(position.getY() + decalValue);
        });
    }
    
    /**
     * Retourne vrai si l'élément donné est touche la form courante à l'est
     * @param formTetris
     * @return 
     */
    public boolean isNeighbourXEAST(FormTetris formTetris){
        return this.listElement.stream().anyMatch((position) -> (formTetris.containsPosition(new Position(position.getX() + 1,position.getY()))));
    }
    
    /**
     * Retourne vrai si l'élément donné est touche la form courante à l'ouest
     * @param formTetris
     * @return 
     */
    public boolean isNeighbourXWEST(FormTetris formTetris){
        return this.listElement.stream().anyMatch((position) -> (formTetris.containsPosition(new Position(position.getX() - 1,position.getY()))));
    }
    
    /**
     * Retourne vrai si l'élément donné est touche la form courante au nord
     * @param formTetris
     * @return 
     */
    public boolean isNeighbourXNORTH(FormTetris formTetris){
        return this.listElement.stream().anyMatch((position) -> (formTetris.containsPosition(new Position(position.getX(),position.getY() - 1))));
    }
    
    /**
     * Retourne vrai si l'élément donné est touche la form courante au sud
     * @param formTetris
     * @return 
     */
    public boolean isNeighbourXSOUTH(FormTetris formTetris){
        return this.listElement.stream().anyMatch((position) -> (formTetris.containsPosition(new Position(position.getX(),position.getY() + 1))));
    }

    public ArrayList<Position> getListElement() {
        return listElement;
    }
}
