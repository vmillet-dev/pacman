/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package outils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import modele.Case;
import modele.Couloir;
import modele.Mur;
import modele.Position;

/**
 *
 * @author Florent
 */
public class TetrisBuild {
    
    
    /*
        Longueur du plateau
    */
    private int length;
    
    /*
        Largeur du plateau
    */
    private int width;
    
    /**
     * Nomnre de super pacgomme demandé
     */
    private int nbSuperPacGomme;

    public TetrisBuild(int length, int width, int nbSuperPacGomme) {
        this.length = length;
        this.width = width;
        this.nbSuperPacGomme = nbSuperPacGomme;
    }

    public Case[][] build() {
        Case[][] tableau = new Case[length][width];
        
        ArrayList<Position> positionDispo = new ArrayList<>();
        for (int x = 0; x < this.getFixLength(this.length); x++) {
            for (int y = 0; y < this.getFixLength(this.width); y++) {
                positionDispo.add(new Position(x, y));
            }
        }

        for (int x = 0; x < this.length; x++) {
            for (int y = 0; y < this.width; y++) {
                if (x == 0 || x == length - 1 || y == 0 || y == width - 1) {
                    tableau[x][y] = new Mur();
                } else {
                    tableau[x][y] = new Couloir();
                }
            }
        }

        ArrayList<FormTetris> listFormTetris = this.getRandomFormTeris(positionDispo);

        this.heigthAdjustements(listFormTetris);
        this.widthAdjustements(listFormTetris);

        for (FormTetris formTetris : listFormTetris) {
            for (Position position : formTetris.getListElement()) {
                tableau[2 + (position.getX())][2 + (position.getY())] = new Mur();
            }
        }

        // miroir bas gauche
        for (int x = 1; x < this.length; x++) {
            for (int y = width / 2; y < this.width; y++) {
                if (tableau[x][width - y - 1] instanceof Mur) {
                    tableau[x][y] = new Mur();
                } else {
                    tableau[x][y] = new Couloir();
                }
            }
        }

        for (int x = this.length / 2; x < this.length; x++) {
            for (int y = 1; y < this.width; y++) {
                if (tableau[length - x - 1][y] instanceof Mur) {
                    tableau[x][y] = new Mur();
                } else {
                    tableau[x][y] = new Couloir();
                }
            }
        }

        // génération pacgomme/superPacGomme
        ArrayList<Couloir> listCouloir = new ArrayList<>();
        for (int x = 0; x < this.length; x++) {
            for (int y = 0; y < this.width; y++) {
                if (tableau[x][y] instanceof Couloir) {
                    listCouloir.add((Couloir) tableau[x][y]);
                    ((Couloir) tableau[x][y]).setPacGomme(true);
                }
            }
        }

        for (int i = 0; i < this.nbSuperPacGomme; i++) {
            Couloir temp = ((Couloir) listCouloir.remove(Tool.monRandom(0, listCouloir.size())));
            if(temp.isSuperPacGomme())
                i--;
            else{
                temp.setPacGomme(false);
                temp.setSuperPacGomme(true);
            }
        }
        
        int positionY = Tool.monRandom(1, this.width -1);
        tableau[0][positionY] = new Couloir();
        tableau[this.length - 1][positionY] = new Couloir();
        
        return tableau;
    }

    /**
     * Correction manuel de la longueur
     * @param value
     * @return 
     */
    private int getFixLength(int value) {
        int fixValue = value / 4;
        return fixValue + (fixValue % 2 == 0 ? 4 : 3);
    }

    
    /**
     * Fonction récursive placant des formTetris dans un espace donné
     * @param positionDispo liste des positions disponible
     * @return  liste des formes tetris
     */
    private ArrayList<FormTetris> getRandomFormTeris(ArrayList<Position> positionDispo) {

        ArrayList<FormTetris> listFormTetris = new ArrayList<>();

        if (positionDispo.isEmpty()) {
            return listFormTetris;
        }

        positionDispo.sort((Position pos1, Position pos2) -> {
            if (pos1.getY() > pos2.getY()) {
                return 1;
            }
            if (pos1.getY() < pos2.getY()) {
                return -1;
            }
            if (pos1.getX() > pos2.getX()) {
                return 1;
            }
            if (pos1.getX() < pos2.getX()) {
                return -1;
            }
            return 0;
        });
        Position position0 = positionDispo.get(0);
        ArrayList<FormTetris> listFormTetrisDispo = this.getAllForm(position0);
        listFormTetrisDispo.removeIf((FormTetris e) -> {
            return !e.isInsertable(positionDispo);
        });

        FormTetris formTetris;
        if (listFormTetrisDispo.isEmpty()) {
            formTetris = FormTetris.getSimpleFormPoint(position0.getX(), position0.getY());
        } else {
            formTetris = listFormTetrisDispo.get(Tool.monRandom(0, listFormTetrisDispo.size()));
        }

        listFormTetris.add(formTetris);

        positionDispo.removeIf((Position e) -> {
            return formTetris.containsPosition(e);
        });
        listFormTetris.addAll(this.getRandomFormTeris(positionDispo));

        return listFormTetris;

    }
    
    /**
     * Obtention de toutes les formes disponibles à partir d'un point donné
     * @param pointEncrage
     * @return 
     */
    private ArrayList<FormTetris> getAllForm(Position pointEncrage) {
        FormTetris formTetris;
        ArrayList<FormTetris> allForm = new ArrayList<>();

        // forme en L 
        ArrayList<FormTetris> formL = new ArrayList<>();

        // taille de 1 à 3
        for (int size = 1; size < 4; size++) {
            formTetris = FormTetris.getFormL(pointEncrage.getX(), pointEncrage.getY(), size);
            formL.add(formTetris);
            formL.add(formTetris.getSysmetriqueBas());
            formL.add(formTetris.getSysmetriqueDroite());
            formL.add(formTetris.getSysmetriqueBas().getSysmetriqueDroite());
        }
        allForm.addAll(formL);

        allForm.removeIf((FormTetris e) -> {
            return !e.containsPosition(pointEncrage);
        });
        return allForm;
    }

    /**
     * Ecartement des pièces en Y
     * @param listForm 
     */
    public void heigthAdjustements(ArrayList<FormTetris> listForm) {

        // récuperation de tout les voisins
        Set<FormTetris> neighbour = new HashSet<>();
        listForm.forEach((formTetris) -> {
            neighbour.addAll(this.getNeighbourHeight(formTetris, listForm));
        });
        if (!neighbour.isEmpty()) {
            this.decalNeighbourY(new ArrayList(neighbour), 1);
            this.heigthAdjustements(listForm);
        }
    }

    /**
     * Ecartement des pièces en X
     * @param listForm 
     */
    public void widthAdjustements(ArrayList<FormTetris> listForm) {

        // récuperation de tout les voisins
        Set<FormTetris> neighbour = new HashSet<>();
        listForm.forEach((formTetris) -> {
            neighbour.addAll(this.getNeighbourWidth(formTetris, listForm));
        });
        if (!neighbour.isEmpty()) {
            this.decalNeighbourX(new ArrayList(neighbour), 1);
            this.widthAdjustements(listForm);
        }
    }

    /**
     * Décale tout les éléments en Y d'une valeur donné 
     * @param listNeighbour
     * @param value 
     */
    private void decalNeighbourY(ArrayList<FormTetris> listNeighbour, int value) {
        listNeighbour.forEach((formTetris) -> {
            formTetris.decalY(value);
        });
    }

    /**
     * Décale tout les éléments en X d'une valeur donné 
     * @param listNeighbour
     * @param value 
     */
    private void decalNeighbourX(ArrayList<FormTetris> listNeighbour, int value) {
        listNeighbour.forEach((formTetris) -> {
            formTetris.decalX(value);
        });
    }
    
    
    /**
     * Récupère tout les éléments en dessous de l'élément ciblé
     * @param formTetris
     * @param listForm
     * @return 
     */
    private ArrayList<FormTetris> getNeighbourHeight(FormTetris formTetris, ArrayList<FormTetris> listForm) {
        ArrayList<FormTetris> neighbour = new ArrayList<>();
        listForm.stream().filter((form) -> (formTetris.isNeighbourXSOUTH(form) && form != formTetris)).forEachOrdered((form) -> {
            neighbour.add(form);
        });
        return neighbour;
    }

    /**
     * Récupère tout les éléments à droite de l'élément ciblé
     * @param formTetris
     * @param listForm
     * @return 
     */
    private ArrayList<FormTetris> getNeighbourWidth(FormTetris formTetris, ArrayList<FormTetris> listForm) {
        ArrayList<FormTetris> neighbour = new ArrayList<>();
        listForm.stream().filter((form) -> (formTetris.isNeighbourXEAST(form) && form != formTetris)).forEachOrdered((form) -> {
            neighbour.add(form);
        });
        return neighbour;
    }

}
