/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele;

/**
 *
 * @author p1601511
 */
public class Position {

    private int x;
    private int y;

    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }
   
    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
    
    public void setPositionWithDirection(Direction direction)
    {
        this.x += (Direction.EST == direction ? 1 : 0)+( Direction.OUEST == direction ? -1 : 0);
        this.y += (Direction.SUD == direction ? 1 : 0)+( Direction.NORD == direction ? -1 : 0);
    }

    @Override
    public Position clone() {
        return new Position(this.x,this.y);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        if (this.x != ((Position)obj).x)
            return false;
        if (this.y != ((Position)obj).y)
            return false;
        return true;
    }
}
