/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author p1601511
 */
public class SablierSuperPacGomme {

    private Thread threadSablier;
    private Integer time;
    private int killEntite;

    public SablierSuperPacGomme() {
        
        /*
            définition d'un thread qui décrement en boucle la variable time 
            tant que celle ci est > 0
        */
        this.threadSablier = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    while (time > 0) {
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException ex) {
                            Logger.getLogger(SablierSuperPacGomme.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        time--;
                    }
                }
            }
        });
        this.time = 0;
        this.killEntite = 0;
    }

    public Thread getThreadSablier() {
        return threadSablier;
    }

    /**
     * 
     * @return true si le timer est actif false sinon
     */
    public boolean isActive() {
        return this.time > 0;
    }

    /**
     * Relance le timer et met à 0 le compteur d'entité tué(s)
     */
    public void restart() {
        this.time = 10;
        this.killEntite = 0;

    }

    public void start() {
        this.time = 10;
        this.killEntite = 0;
        this.threadSablier.start();
    }

    public int getKillEntite() {
        return killEntite;
    }

    public void incrementKillEntite() {
        this.killEntite++;
    }

    public void stop() {
        this.time = 0;
    }

}
