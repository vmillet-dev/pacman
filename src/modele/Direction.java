/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele;

/**
 *
 * @author p1601511
 */
public enum Direction {
    NORD,
    SUD,
    EST,
    OUEST,
    AUCUNE;
    
    public static Direction getDirection(Position position1,Position position2){
        if((Math.abs(position1.getX() - position2.getX()) == 0 
                && Math.abs(position1.getY() - position2.getY()) == 1) || (
                Math.abs(position1.getX() - position2.getX()) == 1 
                && Math.abs(position1.getY() - position2.getY()) == 0))
        {
            if(Math.abs(position1.getX() - position2.getX()) == 0 
                    && Math.abs(position1.getY() - position2.getY()) == 1)
                return position1.getY() - position2.getY() == 1 ? NORD : SUD;
            else
                return position1.getX() - position2.getX() == 1 ? OUEST : EST;
        }
        else
            return Direction.AUCUNE;
    }
}
