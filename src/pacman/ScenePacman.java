/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pacman;

import java.awt.GraphicsEnvironment;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.paint.Color;
import modele.Couloir;
import modele.Entite;
import modele.Fantome;
import modele.Jeu;
import modele.PacMan;
import modele.Position;

/**
 *
 * @author Florent
 */
public class ScenePacman extends Scene implements Observer {

    private Jeu jeu;
    private GridPane gPaneGame, gPaneScore;

    //Ensemble d'assets de cases chargés en mémoire
    private final Image imgMur, imgPacman, imgPacgum, imgSuperPacgum, imgVie,imgFantomesFuite;
    private final Image[] imgFantomes;
    //Ensemble d'assets d'entités 
    private Map<Entite, ImageView> mapImageView;
    private Map<Couloir, ImageView> mapPacgum;

    private Text tScore;
    private Font font;
    private HBox listeImgVie;

    ScenePacman(BorderPane border, Jeu jeu) {
        super(border, border.getPrefWidth(), border.getPrefHeight());

        this.jeu = jeu;
        this.gPaneGame = (GridPane) border.getBottom();
        this.gPaneScore = (GridPane) border.getTop();
        this.mapImageView = new HashMap<>();
        this.mapPacgum = new HashMap<>();
        this.imgFantomes = new Image[4];
        this.listeImgVie = new HBox();

        this.font = Font.loadFont(getClass().getResource("/fonts/emulogic.ttf").toString(), 20);
        this.tScore = new Text("0");
        this.tScore.setFont(font);
        this.tScore.setFill(Color.WHITE);

        //On charge les assets une seule fois pour la totalité de l'execution du programme
        //Les ImageView ne chargent uniquement ces dernières
        imgMur = new Image(getClass().getResource("/assets/mur.png").toString(), getSizeCell(), getSizeCell(), false, false);
        imgPacman = new Image(getClass().getResource("/assets/pacman-moving.gif").toString(), getSizeCell(), getSizeCell(), false, false);
        imgFantomes[0] = new Image(getClass().getResource("/assets/fantome-bleu.png").toString(), getSizeCell(), getSizeCell(), false, false);
        imgFantomes[1] = new Image(getClass().getResource("/assets/fantome-jaune.png").toString(), getSizeCell(), getSizeCell(), false, false);
        imgFantomes[2] = new Image(getClass().getResource("/assets/fantome-rose.png").toString(), getSizeCell(), getSizeCell(), false, false);
        imgFantomes[3] = new Image(getClass().getResource("/assets/fantome-rouge.png").toString(), getSizeCell(), getSizeCell(), false, false);
        imgPacgum = new Image(getClass().getResource("/assets/pacgum.png").toString(), getSizeCell(), getSizeCell(), false, false);
        imgSuperPacgum = new Image(getClass().getResource("/assets/superpacgum.png").toString(), getSizeCell(), getSizeCell(), false, false);
        imgVie = new Image(getClass().getResource("/assets/pacman.png").toString(), getSizeCell(), getSizeCell(), false, false);
        imgFantomesFuite = new Image(getClass().getResource("/assets/fantomes-fuite.png").toString(), getSizeCell(), getSizeCell(), false, false);
        
        this.initPlateau();
        this.initScoreboard();
    }

    private void initScoreboard() {
        //Changement de la couleur de fond du background en noir
        this.gPaneScore.setStyle("-fx-background-color: #000000;");

        Text tScoreLabel = new Text();
        tScoreLabel.setText("Score: ");
        tScoreLabel.setFont(this.font);
        tScoreLabel.setFill(Color.WHITE);

        Text tVieLabel = new Text();
        tVieLabel.setText("Vie(s): ");
        tVieLabel.setFont(this.font);
        tVieLabel.setFill(Color.WHITE);

        for (int i = 0; i < this.jeu.getPacman().getVie(); i++) {
            listeImgVie.getChildren().add(new ImageView(imgVie));
        }

        this.gPaneScore.add(tVieLabel, 0, 0);
        this.gPaneScore.add(listeImgVie, 1, 0);
        this.gPaneScore.add(tScoreLabel, 2, 0);
        this.gPaneScore.add(tScore, 3, 0);
    }

    private void initPlateau() {
        //Changement de la couleur de fond du background en noir
        this.gPaneGame.setStyle("-fx-background-color: #000000;");

        //Mise en place de l'ensemble des sprites murs
        ImageView imgViewPacgum = null;
        for (int i = 0; i < jeu.getPlateau().length; i++) {
            for (int j = 0; j < jeu.getPlateau()[i].length; j++) {
                if (jeu.getCase(i, j) instanceof modele.Mur) {
                    gPaneGame.add(new ImageView(imgMur), i, j);
                } else {
                    if (((modele.Couloir) jeu.getPlateau()[i][j]).isPacGomme()) {
                        imgViewPacgum = new ImageView(imgPacgum);

                    } else if (((modele.Couloir) jeu.getPlateau()[i][j]).isSuperPacGomme()) {
                        imgViewPacgum = new ImageView(imgSuperPacgum);
                    }
                    if (imgViewPacgum != null) {
                        gPaneGame.add(imgViewPacgum, i, j);
                        mapPacgum.put((Couloir) jeu.getCase(i, j), imgViewPacgum);
                    }
                }
                imgViewPacgum = null;
            }
        }

        Position position;
        Set<Entite> tabEntite = this.jeu.getTabEntite().keySet();
        for (Entite entite : tabEntite) {
            position = this.jeu.getTabEntite().get(entite);
            gPaneGame.add(this.createImageView(entite), position.getX(), position.getY());
        }
    }

    private double getSizeCell() {
        return ((GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds().height - gPaneScore.getPrefHeight())
                / jeu.getMaxWidth());
    }

    @Override
    public synchronized void update(Observable o, Object arg) {
        if (arg instanceof Entite) {

            // On récupère l'ImageView
            ImageView imageViewEntite = this.mapImageView.get((Entite) arg);

            if (imageViewEntite != null) {
                if (arg instanceof modele.PacMan) {
                    switch (((modele.PacMan) arg).getDirection()) {
                            case NORD:
                                imageViewEntite.setRotate(270);
                                break;
                            case EST:
                                imageViewEntite.setRotate(0);
                                break;
                            case SUD:
                                imageViewEntite.setRotate(90);
                                break;
                            case OUEST:
                                imageViewEntite.setRotate(180);
                                break;
                        }
                }

                if (((Entite) arg).getVie() != 0) {
                    //On récupère la position
                    Position position = this.jeu.getTabEntite().get((Entite) arg);

                    //on détermine l'entité en question
                    gPaneGame.setColumnIndex(imageViewEntite, position.getX());
                    gPaneGame.setRowIndex(imageViewEntite, position.getY());
                }
            }
        }

        this.tScore.setText(String.valueOf(this.jeu.getPoint()));
        for (int i = 1; i <= listeImgVie.getChildren().size(); i++) {
            if (i <= this.jeu.getPacman().getVie()) {
                listeImgVie.getChildren().get(i - 1).setVisible(true);
            } else {
                listeImgVie.getChildren().get(i - 1).setVisible(false);
            }
        }
        if(arg instanceof Jeu){
            if(((Jeu)arg).finPartie()){
                ((Jeu)arg).nextLevel();
                this.gPaneGame.getChildren().removeIf((node) -> node instanceof ImageView);
                this.mapImageView.clear();
                this.mapPacgum.clear();
                this.initPlateau();
            }
            this.updateEntite();
            this.updatePacGomme();
        }
    }

    private ImageView createImageView(Entite entite) {
        //Noeud permettant de peindre les sprites des entités
        ImageView imageViewEntite = (entite instanceof Fantome
                ? new ImageView(imgFantomes[outils.Tool.monRandom(0, 4)]) : new ImageView(imgPacman));

        // ajout à la map
        this.mapImageView.put(entite, imageViewEntite);
        return imageViewEntite;
    }

    private void updateEntite() {
        this.jeu.getTabEntite().keySet().forEach((entite) -> {
            if (entite.getVie() == 0) {
                ((ImageView) this.mapImageView.get(entite)).setVisible(false);
            }
            if(entite instanceof Fantome) {
                if(jeu.getSablierSuperPacGomme().isActive()) {
                    ((ImageView) this.mapImageView.get(entite)).setImage(imgFantomesFuite);
                } else {
                    ((ImageView) this.mapImageView.get(entite)).setImage(imgFantomes[outils.Tool.monRandom(0, 4)]);
                }
            }
        });
    }

    private void updatePacGomme() {
        this.mapPacgum.keySet().forEach((couloir) -> {
            if (!couloir.isPacGomme() && !couloir.isSuperPacGomme()) {
                ((ImageView) this.mapPacgum.get(couloir)).setVisible(false);
            }
        });
    }
}
